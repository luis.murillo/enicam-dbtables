package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class TranslateKeyValue {
	private Integer transkv_key;
	private Integer transkv_translate_key;
	private Language language;
	private String transkv_value;
	private boolean transkv_delete;
	private Timestamp transkv_createdat;
	public Integer getTranskv_key() {
		return transkv_key;
	}
	public void setTranskv_key(Integer transkv_key) {
		this.transkv_key = transkv_key;
	}
	public Integer getTranskv_translate_key() {
		return transkv_translate_key;
	}
	public void setTranskv_translate_key(Integer transkv_translate_key) {
		this.transkv_translate_key = transkv_translate_key;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isTranskv_delete() {
		return transkv_delete;
	}
	public void setTranskv_delete(boolean transkv_delete) {
		this.transkv_delete = transkv_delete;
	}
	public Timestamp getTranskv_createdat() {
		return transkv_createdat;
	}
	public void setTranskv_createdat(Timestamp transkv_createdat) {
		this.transkv_createdat = transkv_createdat;
	}
	
	public String getTranskv_value() {
		return transkv_value;
	}
	public void setTranskv_value(String transkv_value) {
		this.transkv_value = transkv_value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((transkv_key == null) ? 0 : transkv_key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslateKeyValue other = (TranslateKeyValue) obj;
		if (transkv_key == null) {
			if (other.transkv_key != null)
				return false;
		} else if (!transkv_key.equals(other.transkv_key))
			return false;
		return true;
	}
	
}
