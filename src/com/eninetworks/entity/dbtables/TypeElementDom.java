package com.eninetworks.entity.dbtables;

public class TypeElementDom {
	private int ted_key;
	private String ted_description;
	public int getTed_key() {
		return ted_key;
	}
	public void setTed_key(int ted_key) {
		this.ted_key = ted_key;
	}
	public String getTed_description() {
		return ted_description;
	}
	public void setTed_description(String ted_description) {
		this.ted_description = ted_description;
	}

	
}
