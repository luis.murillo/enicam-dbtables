package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class User extends Profile{
	private Integer user_userkey;
	private String user_username;
	private String user_password;
	private String user_token;
	private boolean user_deleted;
	private boolean user_enabled;
	private Timestamp user_createdat;
	private Timestamp user_updatedat;
	public Integer getUser_userkey() {
		return user_userkey;
	}
	public void setUser_userkey(Integer user_userkey) {
		this.user_userkey = user_userkey;
	}
	public String getUser_username() {
		return user_username;
	}
	public void setUser_username(String user_username) {
		this.user_username = user_username;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_token() {
		return user_token;
	}
	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}
	public boolean isUser_deleted() {
		return user_deleted;
	}
	public void setUser_deleted(boolean user_deleted) {
		this.user_deleted = user_deleted;
	}
	public boolean isUser_enabled() {
		return user_enabled;
	}
	public void setUser_enabled(boolean user_enabled) {
		this.user_enabled = user_enabled;
	}
	public Timestamp getUser_createdat() {
		return user_createdat;
	}
	public void setUser_createdat(Timestamp user_createdat) {
		this.user_createdat = user_createdat;
	}
	public Timestamp getUser_updatedat() {
		return user_updatedat;
	}
	public void setUser_updatedat(Timestamp user_updatedat) {
		this.user_updatedat = user_updatedat;
	}
}
