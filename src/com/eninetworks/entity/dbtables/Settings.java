package com.eninetworks.entity.dbtables;

public class Settings{
	private Integer sett_settkey;
	private String sett_value;
	private String sett_name;
	private String sett_createdat;
	private String sett_updatedat;
	private String sett_description;
	private boolean sett_public_settings;
	
	public Integer getSett_settkey() {
		return sett_settkey;
	}
	public void setSett_settkey(Integer sett_settkey) {
		this.sett_settkey = sett_settkey;
	}
	public String getSett_value() {
		return sett_value;
	}
	public void setSett_value(String sett_value) {
		this.sett_value = sett_value;
	}
	public String getSett_name() {
		return sett_name;
	}
	public void setSett_name(String sett_name) {
		this.sett_name = sett_name;
	}
	public String getSett_createdat() {
		return sett_createdat;
	}
	public void setSett_createdat(String sett_createdat) {
		this.sett_createdat = sett_createdat;
	}
	public String getSett_updatedat() {
		return sett_updatedat;
	}
	public void setSett_updatedat(String sett_updatedat) {
		this.sett_updatedat = sett_updatedat;
	}
	public String getSett_description() {
		return sett_description;
	}
	public void setSett_description(String sett_description) {
		this.sett_description = sett_description;
	}
	public boolean isSett_public_settings() {
		return sett_public_settings;
	}
	public void setSett_public_settings(boolean sett_public_settings) {
		this.sett_public_settings = sett_public_settings;
	}
}
