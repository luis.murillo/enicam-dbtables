package com.eninetworks.entity.dbtables;

public class Dvr {
	private Integer wad_key;
	private Integer dvr_depth;
	private Integer dvr_lock_days;
	private Integer dvr_space;
	private boolean is_adjustable;
	private boolean is_default;
	private Integer preset_id;
	private String title;
	private boolean is_deleted;
	
	public Integer getWad_key() {
		return wad_key;
	}
	public void setWad_key(Integer wad_key) {
		this.wad_key = wad_key;
	}
	public Integer getDvr_depth() {
		return dvr_depth;
	}
	public void setDvr_depth(Integer dvr_depth) {
		this.dvr_depth = dvr_depth;
	}
	public Integer getDvr_lock_days() {
		return dvr_lock_days;
	}
	public void setDvr_lock_days(Integer dvr_lock_days) {
		this.dvr_lock_days = dvr_lock_days;
	}
	public Integer getDvr_space() {
		return dvr_space;
	}
	public void setDvr_space(Integer dvr_space) {
		this.dvr_space = dvr_space;
	}
	public boolean isIs_adjustable() {
		return is_adjustable;
	}
	public void setIs_adjustable(boolean is_adjustable) {
		this.is_adjustable = is_adjustable;
	}
	public boolean isIs_default() {
		return is_default;
	}
	public void setIs_default(boolean is_default) {
		this.is_default = is_default;
	}
	
	public Integer getPreset_id() {
		return preset_id;
	}
	public void setPreset_id(Integer preset_id) {
		this.preset_id = preset_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isIs_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	
}
