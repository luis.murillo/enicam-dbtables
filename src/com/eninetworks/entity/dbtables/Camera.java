package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Camera extends CameraOptions{
	private Integer cam_camkey;
	private String cam_brand;
	private String cam_model;
	private String cam_serial_key;
	private boolean cam_deleted;
	private String cam_url;
	private Timestamp cam_createdat;
	private Timestamp cam_updatedat;
	private String cam_mac_address;
	private String cam_name;
	
	public Integer getCam_camkey() {
		return cam_camkey;
	}
	public void setCam_camkey(Integer cam_camkey) {
		this.cam_camkey = cam_camkey;
	}
	public String getCam_brand() {
		return cam_brand;
	}
	public void setCam_brand(String cam_brand) {
		this.cam_brand = cam_brand;
	}
	public String getCam_model() {
		return cam_model;
	}
	public void setCam_model(String cam_model) {
		this.cam_model = cam_model;
	}
	public String getCam_serial_key() {
		return cam_serial_key;
	}
	public void setCam_serial_key(String cam_serial_key) {
		this.cam_serial_key = cam_serial_key;
	}
	
	public Timestamp getCam_createdat() {
		return cam_createdat;
	}
	public void setCam_createdat(Timestamp cam_createdat) {
		this.cam_createdat = cam_createdat;
	}
	public Timestamp getCam_updatedat() {
		return cam_updatedat;
	}
	public void setCam_updatedat(Timestamp cam_updatedat) {
		this.cam_updatedat = cam_updatedat;
	}
	public boolean isCam_deleted() {
		return cam_deleted;
	}
	public void setCam_deleted(boolean cam_deleted) {
		this.cam_deleted = cam_deleted;
	}
	public String getCam_url() {
		return cam_url;
	}
	public void setCam_url(String cam_url) {
		this.cam_url = cam_url;
	}
	public String getCam_mac_address() {
		return cam_mac_address;
	}
	public void setCam_mac_address(String cam_mac_address) {
		this.cam_mac_address = cam_mac_address;
	}
	public String getCam_name() {
		return cam_name;
	}
	public void setCam_name(String cam_name) {
		this.cam_name = cam_name;
	}
	
}
