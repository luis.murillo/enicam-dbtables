package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Site_ubication extends Camera {
	private Integer ubic_ubickey;
	private Integer ubic_camkey;
	private Integer ubic_sitekey;
	private String ubic_name;
	private Timestamp ubic_createdat;
	private Timestamp ubic_updatedat;
	private String ubic_preview;//Temporal hasta saber como obtener las img preview de la video-source
	private boolean nuevo;
	private boolean borrado;
	
	public Integer getUbic_ubickey() {
		return ubic_ubickey;
	}
	public void setUbic_ubickey(Integer ubic_ubickey) {
		this.ubic_ubickey = ubic_ubickey;
	}
	public Integer getUbic_camkey() {
		return ubic_camkey;
	}
	public void setUbic_camkey(Integer ubic_camkey) {
		this.ubic_camkey = ubic_camkey;
	}
	public Integer getUbic_sitekey() {
		return ubic_sitekey;
	}
	public void setUbic_sitekey(Integer ubic_sitekey) {
		this.ubic_sitekey = ubic_sitekey;
	}
	public String getUbic_name() {
		return ubic_name;
	}
	public void setUbic_name(String ubic_name) {
		this.ubic_name = ubic_name;
	}
	public Timestamp getUbic_createdat() {
		return ubic_createdat;
	}
	public void setUbic_createdat(Timestamp ubic_createdat) {
		this.ubic_createdat = ubic_createdat;
	}
	public Timestamp getUbic_updatedat() {
		return ubic_updatedat;
	}
	public void setUbic_updatedat(Timestamp ubic_updatedat) {
		this.ubic_updatedat = ubic_updatedat;
	}
	public String getUbic_preview() {
		return ubic_preview;
	}
	public void setUbic_preview(String ubic_preview) {
		this.ubic_preview = ubic_preview;
	}
	public boolean isNuevo() {
		return nuevo;
	}
	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	public boolean isBorrado() {
		return borrado;
	}
	public void setBorrado(boolean borrado) {
		this.borrado = borrado;
	}
	
//	@Override
//	public boolean equals(Object obj) {
//		return (
//			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey()==(this.getSite_sitekey())) && 
//			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey() != null)
//		) || 
//			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey().equals(this.getSite_sitekey())
//		);
//	}
}
