package com.eninetworks.entity.dbtables;

public class Profile {

	private Integer prof_profkey;
	private String prof_name;
	private String prof_description;
	private boolean prof_deleted;
	
	public Integer getProf_profkey() {
		return prof_profkey;
	}
	public void setProf_profkey(Integer prof_profkey) {
		this.prof_profkey = prof_profkey;
	}
	public String getProf_name() {
		return prof_name;
	}
	public void setProf_name(String prof_name) {
		this.prof_name = prof_name;
	}
	public String getProf_description() {
		return prof_description;
	}
	public void setProf_description(String prof_description) {
		this.prof_description = prof_description;
	}
	public boolean isProf_deleted() {
		return prof_deleted;
	}
	public void setProf_deleted(boolean prof_deleted) {
		this.prof_deleted = prof_deleted;
	}
}
