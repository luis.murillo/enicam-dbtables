package com.eninetworks.entity.dbtables;


public class UserCustom extends User {
	private Customer customer;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
