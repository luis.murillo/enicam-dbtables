package com.eninetworks.entity.dbtables;

public class TypeData {
	private int td_key;
	private String td_description;
	
	public int getTd_key() {
		return td_key;
	}
	public void setTd_key(int td_key) {
		this.td_key = td_key;
	}
	public String getTd_description() {
		return td_description;
	}
	public void setTd_description(String td_description) {
		this.td_description = td_description;
	}

}
