package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Language {
	private Integer lang_key;
	private String lang_language;
	private String lang_shortcut;
	private Timestamp lang_createdat;
	private Timestamp lang_updatedat;
	private boolean lang_deleted;
	public Integer getLang_key() {
		return lang_key;
	}
	public void setLang_key(Integer lang_key) {
		this.lang_key = lang_key;
	}
	public String getLang_language() {
		return lang_language;
	}
	public void setLang_language(String lang_language) {
		this.lang_language = lang_language;
	}
	public String getLang_shortcut() {
		return lang_shortcut;
	}
	public void setLang_shortcut(String lang_shortcut) {
		this.lang_shortcut = lang_shortcut;
	}
	public Timestamp getLang_createdat() {
		return lang_createdat;
	}
	public void setLang_createdat(Timestamp lang_createdat) {
		this.lang_createdat = lang_createdat;
	}
	public Timestamp getLang_updatedat() {
		return lang_updatedat;
	}
	public void setLang_updatedat(Timestamp lang_updatedat) {
		this.lang_updatedat = lang_updatedat;
	}
	public boolean isLang_deleted() {
		return lang_deleted;
	}
	public void setLang_deleted(boolean lang_deleted) {
		this.lang_deleted = lang_deleted;
	}
	
}
