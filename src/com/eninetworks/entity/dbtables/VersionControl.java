package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class VersionControl extends VersionControlSO{
	public Integer vers_verskey;
	public String vers_last_version;
	public Timestamp vers_createdat;
	public Timestamp vers_updatedat;
	
	public Integer getVers_verskey() {
		return vers_verskey;
	}
	public void setVers_verskey(Integer vers_verskey) {
		this.vers_verskey = vers_verskey;
	}
	public String getVers_last_version() {
		return vers_last_version;
	}
	public void setVers_last_version(String vers_last_version) {
		this.vers_last_version = vers_last_version;
	}
	public Timestamp getVers_createdat() {
		return vers_createdat;
	}
	public void setVers_createdat(Timestamp vers_createdat) {
		this.vers_createdat = vers_createdat;
	}
	public Timestamp getVers_updatedat() {
		return vers_updatedat;
	}
	public void setVers_updatedat(Timestamp vers_updatedat) {
		this.vers_updatedat = vers_updatedat;
	}
}
