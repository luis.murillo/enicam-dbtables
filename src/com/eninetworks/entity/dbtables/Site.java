package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;
import java.util.List;

import com.eninetworks.entity.dbtables.UbicationSiteCustom;

public class Site extends Plan{
	private Integer site_sitekey;
	private String site_name;
	private String site_address;
	private boolean site_active;
	private List<UbicationSiteCustom> ubications;
	private Timestamp site_createdat;
	private Timestamp site_updatedat;
	private Integer site_subskey;
	private String site_referencia_pago;
	private boolean site_deleted;
	
	
	public Integer getSite_sitekey() {
		return site_sitekey;
	}
	public void setSite_sitekey(Integer site_sitekey) {
		this.site_sitekey = site_sitekey;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getSite_address() {
		return site_address;
	}
	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public Timestamp getSite_createdat() {
		return site_createdat;
	}
	public void setSite_createdat(Timestamp site_createdat) {
		this.site_createdat = site_createdat;
	}
	public Timestamp getSite_updatedat() {
		return site_updatedat;
	}
	public void setSite_updatedat(Timestamp site_updatedat) {
		this.site_updatedat = site_updatedat;
	}

	public List<UbicationSiteCustom> getUbications() {
		return ubications;
	}
	public void setUbications(List<UbicationSiteCustom> ubications) {
		this.ubications = ubications;
	}
	public boolean isSite_active() {
		return site_active;
	}
	public void setSite_active(boolean site_active) {
		this.site_active = site_active;
	}
	
	public Integer getSite_subskey() {
		return site_subskey;
	}
	public void setSite_subskey(Integer site_subskey) {
		this.site_subskey = site_subskey;
	}
	
	public String getSite_referencia_pago() {
		return site_referencia_pago;
	}
	public void setSite_referencia_pago(String site_referencia_pago) {
		this.site_referencia_pago = site_referencia_pago;
	}
	
	public boolean isSite_deleted() {
		return site_deleted;
	}
	public void setSite_deleted(boolean site_deleted) {
		this.site_deleted = site_deleted;
	}
	@Override
	public boolean equals(Object obj) {
		return (
			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey()==(this.getSite_sitekey())) && 
			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey() != null)
		) || 
			(obj != null && obj instanceof Site && ((Site) obj).getSite_sitekey().equals(this.getSite_sitekey())
		);
	}
}
