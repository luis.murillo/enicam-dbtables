package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Customer extends User {
	private Integer cust_custkey;
	private String cust_firstname;
	private String cust_lastname;
	private boolean cust_deleted;
	private boolean cust_enabled;
	private Timestamp cust_createdat;
	private Timestamp cust_updatedat;
	private String cust_folio_comercial;
	private String cust_email;
	private boolean cust_network_control;
	
	public Integer getCust_custkey() {
		return cust_custkey;
	}
	public void setCust_custkey(Integer cust_custkey) {
		this.cust_custkey = cust_custkey;
	}
	public String getCust_firstname() {
		return cust_firstname;
	}
	public void setCust_firstname(String cust_firstname) {
		this.cust_firstname = cust_firstname;
	}
	public String getCust_lastname() {
		return cust_lastname;
	}
	public void setCust_lastname(String cust_lastname) {
		this.cust_lastname = cust_lastname;
	}
	public boolean isCust_deleted() {
		return cust_deleted;
	}
	public void setCust_deleted(boolean cust_deleted) {
		this.cust_deleted = cust_deleted;
	}
	public boolean isCust_enabled() {
		return cust_enabled;
	}
	public void setCust_enabled(boolean cust_enabled) {
		this.cust_enabled = cust_enabled;
	}
	public Timestamp getCust_createdat() {
		return cust_createdat;
	}
	public void setCust_createdat(Timestamp cust_createdat) {
		this.cust_createdat = cust_createdat;
	}
	public Timestamp getCust_updatedat() {
		return cust_updatedat;
	}
	public void setCust_updatedat(Timestamp cust_updatedat) {
		this.cust_updatedat = cust_updatedat;
	}
	public String getCust_folio_comercial() {
		return cust_folio_comercial;
	}
	public void setCust_folio_comercial(String cust_folio_comercial) {
		this.cust_folio_comercial = cust_folio_comercial;
	}
	public String getCust_email() {
		return cust_email;
	}
	public void setCust_email(String cust_email) {
		this.cust_email = cust_email;
	}
	public boolean isCust_network_control() {
		return cust_network_control;
	}
	public void setCust_network_control(boolean cust_network_control) {
		this.cust_network_control = cust_network_control;
	}
	
}
