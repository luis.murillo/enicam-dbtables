package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Subscription extends SubscriptionStatus{
	private Integer subs_subskey;
	private Integer subs_custkey;
	private Integer subs_plankey;
	private Integer subs_sitekey;
	private Integer subs_statuskey;
	private Timestamp subs_startdat;
	private Timestamp subs_enddat;
	private Timestamp subs_createdat;
	private Timestamp subs_updatedat;
	
	public Integer getSubs_subskey() {
		return subs_subskey;
	}
	public void setSubs_subskey(Integer subs_subskey) {
		this.subs_subskey = subs_subskey;
	}
	public Integer getSubs_custkey() {
		return subs_custkey;
	}
	public void setSubs_custkey(Integer subs_custkey) {
		this.subs_custkey = subs_custkey;
	}
	public Integer getSubs_plankey() {
		return subs_plankey;
	}
	public void setSubs_plankey(Integer subs_plankey) {
		this.subs_plankey = subs_plankey;
	}
	public Integer getSubs_sitekey() {
		return subs_sitekey;
	}
	public void setSubs_sitekey(Integer subs_sitekey) {
		this.subs_sitekey = subs_sitekey;
	}
	public Timestamp getSubs_startdat() {
		return subs_startdat;
	}
	public void setSubs_startdat(Timestamp subs_startdat) {
		this.subs_startdat = subs_startdat;
	}
	public Timestamp getSubs_enddat() {
		return subs_enddat;
	}
	public void setSubs_enddat(Timestamp subs_enddat) {
		this.subs_enddat = subs_enddat;
	}
	public Timestamp getSubs_createdat() {
		return subs_createdat;
	}
	public void setSubs_createdat(Timestamp subs_createdat) {
		this.subs_createdat = subs_createdat;
	}
	public Timestamp getSubs_updatedat() {
		return subs_updatedat;
	}
	public void setSubs_updatedat(Timestamp subs_updatedat) {
		this.subs_updatedat = subs_updatedat;
	}
	public Integer getSubs_statuskey() {
		return subs_statuskey;
	}
	public void setSubs_statuskey(Integer subs_statuskey) {
		this.subs_statuskey = subs_statuskey;
	}
	
}
