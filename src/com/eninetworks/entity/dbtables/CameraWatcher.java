package com.eninetworks.entity.dbtables;

public class CameraWatcher extends Camera{

	private String title;
	private String stream_url;
	private String substream_url;
	private String thumbnails_url;
	private String agent_serial;
	private String name;
	private String ubication_name;
	private boolean enabled;
	private boolean stati;
	private boolean thumbnails;
	private boolean vision_enabled;
	private boolean onvif_ptz;
	private boolean motion_detector_enabled;
	private boolean video_only;
	private Integer organization_id;
	private Integer preset_id;
	private Integer folder_id;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStream_url() {
		return stream_url;
	}
	public void setStream_url(String stream_url) {
		this.stream_url = stream_url;
	}
	public String getSubstream_url() {
		return substream_url;
	}
	public void setSubstream_url(String substream_url) {
		this.substream_url = substream_url;
	}
	public String getThumbnails_url() {
		return thumbnails_url;
	}
	public void setThumbnails_url(String thumbnails_url) {
		this.thumbnails_url = thumbnails_url;
	}
	public String getAgent_serial() {
		return agent_serial;
	}
	public void setAgent_serial(String agent_serial) {
		this.agent_serial = agent_serial;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isStati() {
		return stati;
	}
	public void setStati(boolean stati) {
		this.stati = stati;
	}
	public boolean isThumbnails() {
		return thumbnails;
	}
	public void setThumbnails(boolean thumbnails) {
		this.thumbnails = thumbnails;
	}
	public boolean isVision_enabled() {
		return vision_enabled;
	}
	public void setVision_enabled(boolean vision_enabled) {
		this.vision_enabled = vision_enabled;
	}
	public boolean isOnvif_ptz() {
		return onvif_ptz;
	}
	public void setOnvif_ptz(boolean onvif_ptz) {
		this.onvif_ptz = onvif_ptz;
	}
	public boolean isMotion_detector_enabled() {
		return motion_detector_enabled;
	}
	public void setMotion_detector_enabled(boolean motion_detector_enabled) {
		this.motion_detector_enabled = motion_detector_enabled;
	}
	public boolean isVideo_only() {
		return video_only;
	}
	public void setVideo_only(boolean video_only) {
		this.video_only = video_only;
	}
	public Integer getOrganization_id() {
		return organization_id;
	}
	public void setOrganization_id(Integer organization_id) {
		this.organization_id = organization_id;
	}
	public Integer getPreset_id() {
		return preset_id;
	}
	public void setPreset_id(Integer preset_id) {
		this.preset_id = preset_id;
	}
	public Integer getFolder_id() {
		return folder_id;
	}
	public void setFolder_id(Integer folder_id) {
		this.folder_id = folder_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUbication_name() {
		return ubication_name;
	}
	public void setUbication_name(String ubication_name) {
		this.ubication_name = ubication_name;
	}	
}
