package com.eninetworks.entity.dbtables;

public class CameraOptions {
	private Integer opt_optkey;
	private boolean opt_can_move;
	private boolean opt_can_speak;
	private boolean opt_can_rec_sound;
	private String opt_description;
	
	public Integer getOpt_optkey() {
		return opt_optkey;
	}
	public void setOpt_optkey(Integer opt_optkey) {
		this.opt_optkey = opt_optkey;
	}
	public boolean isOpt_can_speak() {
		return opt_can_speak;
	}
	public void setOpt_can_speak(boolean opt_can_speak) {
		this.opt_can_speak = opt_can_speak;
	}
	public boolean isOpt_can_move() {
		return opt_can_move;
	}
	public void setOpt_can_move(boolean opt_can_move) {
		this.opt_can_move = opt_can_move;
	}
	public boolean isOpt_can_rec_sound() {
		return opt_can_rec_sound;
	}
	public void setOpt_can_rec_sound(boolean opt_can_rec_sound) {
		this.opt_can_rec_sound = opt_can_rec_sound;
	}
	public String getOpt_description() {
		return opt_description;
	}
	public void setOpt_description(String opt_description) {
		this.opt_description = opt_description;
	}
}
