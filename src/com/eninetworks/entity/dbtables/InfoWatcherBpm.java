package com.eninetworks.entity.dbtables;

public class InfoWatcherBpm {
	private String folio_comercial;
	private int admin_id;
	private int organization_id;
	private int user_id;
	private String user_username;
	private String user_password;
	private String session;
	private String referencia_pago;
	
	public String getFolio_comercial() {
		return folio_comercial;
	}
	public void setFolio_comercial(String folio_comercial) {
		this.folio_comercial = folio_comercial;
	}
	public int getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}
	public int getOrganization_id() {
		return organization_id;
	}
	public void setOrganization_id(int organization_id) {
		this.organization_id = organization_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_username() {
		return user_username;
	}
	public void setUser_username(String user_username) {
		this.user_username = user_username;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getReferencia_pago() {
		return referencia_pago;
	}
	public void setReferencia_pago(String referencia_pago) {
		this.referencia_pago = referencia_pago;
	}	

}
