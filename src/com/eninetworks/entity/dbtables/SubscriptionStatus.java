package com.eninetworks.entity.dbtables;

public class SubscriptionStatus {
	private Integer st_key ;
	private String st_description ;
	private String st_clave;
	
	public Integer getSt_key() {
		return st_key;
	}
	public void setSt_key(Integer st_key) {
		this.st_key = st_key;
	}
	public String getSt_description() {
		return st_description;
	}
	public void setSt_description(String st_description) {
		this.st_description = st_description;
	}
	public String getSt_clave() {
		return st_clave;
	}
	public void setSt_clave(String st_clave) {
		this.st_clave = st_clave;
	}
	
	
}
