package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class VersionControlSO {

	public Integer veso_vesokey;
	public String veso_name;
	public Timestamp veso_createdat;
	public Timestamp veso_updatedat;
	
	public Integer getVeso_vesokey() {
		return veso_vesokey;
	}
	public void setVeso_vesokey(Integer veso_vesokey) {
		this.veso_vesokey = veso_vesokey;
	}
	public String getVeso_name() {
		return veso_name;
	}
	public void setVeso_name(String veso_name) {
		this.veso_name = veso_name;
	}
	public Timestamp getVeso_createdat() {
		return veso_createdat;
	}
	public void setVeso_createdat(Timestamp veso_createdat) {
		this.veso_createdat = veso_createdat;
	}
	public Timestamp getVeso_updatedat() {
		return veso_updatedat;
	}
	public void setVeso_updatedat(Timestamp veso_updatedat) {
		this.veso_updatedat = veso_updatedat;
	}
	
	
	
}
