package com.eninetworks.entity.dbtables;

public class SendMailAccountBPM {

	private User user;
	private Customer customer;
	private Plan currentPlan;
	private Plan changeToPlan;
	private String oldEmail;
	private String newEmail;
	private String referenciapago;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Plan getCurrentPlan() {
		return currentPlan;
	}
	public void setCurrentPlan(Plan currentPlan) {
		this.currentPlan = currentPlan;
	}
	public Plan getChangeToPlan() {
		return changeToPlan;
	}
	public void setChangeToPlan(Plan changeToPlan) {
		this.changeToPlan = changeToPlan;
	}
	public String getOldEmail() {
		return oldEmail;
	}
	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}
	public String getNewEmail() {
		return newEmail;
	}
	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}
	public String getReferenciapago() {
		return referenciapago;
	}
	public void setReferenciapago(String referenciapago) {
		this.referenciapago = referenciapago;
	}
	
	
	
}
