package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class NetworkControl {
	public Integer netw_netwkey;
	public String netw_ip_address;
	public String netw_description;
	public Timestamp netw_createdat;
	public Timestamp netw_updatedat;
	private boolean netw_enabled;
	private boolean netw_deleted; 
	public Integer getNetw_netwkey() {
		return netw_netwkey;
	}
	public void setNetw_netwkey(Integer netw_netwkey) {
		this.netw_netwkey = netw_netwkey;
	}
	public String getNetw_ip_address() {
		return netw_ip_address;
	}
	public void setNetw_ip_address(String netw_ip_address) {
		this.netw_ip_address = netw_ip_address;
	}
	public String getNetw_description() {
		return netw_description;
	}
	public void setNetw_description(String netw_description) {
		this.netw_description = netw_description;
	}
	public Timestamp getNetw_createdat() {
		return netw_createdat;
	}
	public void setNetw_createdat(Timestamp netw_createdat) {
		this.netw_createdat = netw_createdat;
	}
	public Timestamp getNetw_updatedat() {
		return netw_updatedat;
	}
	public void setNetw_updatedat(Timestamp netw_updatedat) {
		this.netw_updatedat = netw_updatedat;
	}
	public boolean isNetw_enabled() {
		return netw_enabled;
	}
	public void setNetw_enabled(boolean netw_enabled) {
		this.netw_enabled = netw_enabled;
	}
	public boolean isNetw_deleted() {
		return netw_deleted;
	}
	public void setNetw_deleted(boolean netw_deleted) {
		this.netw_deleted = netw_deleted;
	}
}
