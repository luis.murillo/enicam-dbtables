package com.eninetworks.entity.dbtables;

public class ListenerEnitvBpm {
	private Long id_orden_servicio;
	private String referencia_pago;
	private String folio_comercial;
	private Customer customer;
	private Plan plan;
	private String status_servicio;
	private String status_producto;
	private Boolean estatus_producto;
	private String direccion;
	private String cancel_tipo_solicitud;
	private String cancel_clave_prioridad;
	private int cancel_id_motivo;
	private int id_plan_with_paquete;
	public Long getId_orden_servicio() {
		return id_orden_servicio;
	}
	public void setId_orden_servicio(Long id_orden_servicio) {
		this.id_orden_servicio = id_orden_servicio;
	}
	public String getReferencia_pago() {
		return referencia_pago;
	}
	public void setReferencia_pago(String referencia_pago) {
		this.referencia_pago = referencia_pago;
	}
	public String getFolio_comercial() {
		return folio_comercial;
	}
	public void setFolio_comercial(String folio_comercial) {
		this.folio_comercial = folio_comercial;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public String getStatus_servicio() {
		return status_servicio;
	}
	public void setStatus_servicio(String status_servicio) {
		this.status_servicio = status_servicio;
	}
	public Boolean getEstatus_producto() {
		return estatus_producto;
	}
	public void setEstatus_producto(Boolean estatus_producto) {
		this.estatus_producto = estatus_producto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getStatus_producto() {
		return status_producto;
	}
	public void setStatus_producto(String status_producto) {
		this.status_producto = status_producto;
	}
	public String getCancel_tipo_solicitud() {
		return cancel_tipo_solicitud;
	}
	public void setCancel_tipo_solicitud(String cancel_tipo_solicitud) {
		this.cancel_tipo_solicitud = cancel_tipo_solicitud;
	}
	public String getCancel_clave_prioridad() {
		return cancel_clave_prioridad;
	}
	public void setCancel_clave_prioridad(String cancel_clave_prioridad) {
		this.cancel_clave_prioridad = cancel_clave_prioridad;
	}
	public int getCancel_id_motivo() {
		return cancel_id_motivo;
	}
	public void setCancel_id_motivo(int cancel_id_motivo) {
		this.cancel_id_motivo = cancel_id_motivo;
	}
	public int getId_plan_with_paquete() {
		return id_plan_with_paquete;
	}
	public void setId_plan_with_paquete(int id_plan_with_paquete) {
		this.id_plan_with_paquete = id_plan_with_paquete;
	}	
}
