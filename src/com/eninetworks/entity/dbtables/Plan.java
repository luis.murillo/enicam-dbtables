package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;

public class Plan extends Dvr {
	private Integer plan_plankey;
	private String plan_name;
	private Integer plan_cam_count;
	private Timestamp plan_createdat;
	private Timestamp plan_updatedat;
	private double plan_price;
	private boolean plan_deleted;
	
	public Integer getPlan_plankey() {
		return plan_plankey;
	}
	public void setPlan_plankey(Integer plan_plankey) {
		this.plan_plankey = plan_plankey;
	}
	public String getPlan_name() {
		return plan_name;
	}
	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}
	public Integer getPlan_cam_count() {
		return plan_cam_count;
	}
	public void setPlan_cam_count(Integer plan_cam_count) {
		this.plan_cam_count = plan_cam_count;
	}
	public Timestamp getPlan_createdat() {
		return plan_createdat;
	}
	public void setPlan_createdat(Timestamp plan_createdat) {
		this.plan_createdat = plan_createdat;
	}
	public Timestamp getPlan_updatedat() {
		return plan_updatedat;
	}
	public void setPlan_updatedat(Timestamp plan_updatedat) {
		this.plan_updatedat = plan_updatedat;
	}
	public double getPlan_price() {
		return plan_price;
	}
	public void setPlan_price(double plan_price) {
		this.plan_price = plan_price;
	}
	public boolean isPlan_deleted() {
		return plan_deleted;
	}
	public void setPlan_deleted(boolean plan_deleted) {
		this.plan_deleted = plan_deleted;
	}
	
}
