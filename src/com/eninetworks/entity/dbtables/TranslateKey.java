package com.eninetworks.entity.dbtables;

import java.sql.Timestamp;
import java.util.List;

public class TranslateKey {
	private Integer transk_key;
	private String transk_description;
	private Timestamp transk_createdat;
	private Timestamp transk_updatedat;
	private boolean transk_deleted;
	private List<TranslateKeyValue> lstTranslateKeyValue;
	public Integer getTransk_key() {
		return transk_key;
	}
	public void setTransk_key(Integer transk_key) {
		this.transk_key = transk_key;
	}
	public String getTransk_description() {
		return transk_description;
	}
	public void setTransk_description(String transk_description) {
		this.transk_description = transk_description;
	}
	public Timestamp getTransk_createdat() {
		return transk_createdat;
	}
	public void setTransk_createdat(Timestamp transk_createdat) {
		this.transk_createdat = transk_createdat;
	}
	public Timestamp getTransk_updatedat() {
		return transk_updatedat;
	}
	public void setTransk_updatedat(Timestamp transk_updatedat) {
		this.transk_updatedat = transk_updatedat;
	}
	public boolean isTransk_deleted() {
		return transk_deleted;
	}
	public void setTransk_deleted(boolean transk_deleted) {
		this.transk_deleted = transk_deleted;
	}
	public List<TranslateKeyValue> getLstTranslateKeyValue() {
		return lstTranslateKeyValue;
	}
	public void setLstTranslateKeyValue(List<TranslateKeyValue> lstTranslateKeyValue) {
		this.lstTranslateKeyValue = lstTranslateKeyValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((transk_key == null) ? 0 : transk_key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslateKey other = (TranslateKey) obj;
		if (transk_key == null) {
			if (other.transk_key != null)
				return false;
		} else if (!transk_key.equals(other.transk_key))
			return false;
		return true;
	}
	
}
