package com.eninetworks.entity.dbtables;

public class SiteExtraInfo {
	private Integer camCount;

	public Integer getCamCount() {
		return camCount;
	}

	public void setCamCount(Integer camCount) {
		this.camCount = camCount;
	}
}
