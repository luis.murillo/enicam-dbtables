package com.eninetworks.entity.dbtables;

public class SettingsCustom extends Settings{

	private TypeData type_data;
	private TypeElementDom type_element_dom;
	
	public TypeData getType_data() {
		return type_data;
	}
	public void setType_data(TypeData type_data) {
		this.type_data = type_data;
	}
	public TypeElementDom getType_element_dom() {
		return type_element_dom;
	}
	public void setType_element_dom(TypeElementDom type_element_dom) {
		this.type_element_dom = type_element_dom;
	}
	
}
